��    G      T  a   �                  2     S     _  "   u  	   �     �     �  	   �     �     �     �  
   �           	               2     A  
   R     ]     {     �     �     �     �     �       	   )     3     ;  
   G  
   R     ]     d      w  	   �     �     �     �     �     �     	     '	  '   >	  
   f	  )   q	     �	  "   �	      �	  $   �	  
   
     
     &
     ;
  +   J
     v
     }
     �
     �
     �
     �
  	   �
  ,   �
     �
     �
               +     1  W  5  2   �  )   �     �      �  -     
   J     U     j     �  
   �     �  
   �  
   �     �     �     �  $   �     �          $     ,     K     `      v     �     �     �     �  	                  '     3     ;     C  %   U     {     �     �     �  #   �     �     �       2   ,  
   _  7   j     �  (   �     �  *   �  
   *  	   5     ?     U  3   f     �     �     �     �     �     �  	   �  5   �       #   *     N     W     e     i         +   '         E   
          (   /   #   .                      ,            8                        6       &      )   $   F           2          B   A   >      ?   0   <                      *   !          -   %      C   ;           9      =                  "          G   :   1   @      7       4          3      5   	       D                      '%s' will be set as default User A P P S T O R E  S E T T I N G S ADD ACCOUNT Authentication Failed B L U E T O O T H  S E T T I N G S BT is OFF Bluetooth Adapter Added Bluetooth Adapter Removed C L O S E CLOSE CONNECT CardName CardNumber CardType City Country Create a new user account ? DELETE ACCOUNT DELETE FROM LIST DISCONNECT Delete '%s' Account UserId  ? Device "%s" Disconnected Device "%s" connected Device Failed to connect Disable Multi-touch Do Not Leave Blank Entry Do You want a Device Scan? Do you want a network Search? E-mail Id ENGLISH ExpiryMonth ExpiryYear First Name GERMAN Incorrect Password L A N G U A G E  S E T T I N G S Last Name Multi-touch with 2 points Multi-touch with 3 points N O Network "%s" Already Connected Network "%s" Disconnected Network "%s" Failed Network "%s" connected Network "%s" is ready but not connected Newsletter Not able to pair! Please check the device PAIR NEW DEVICE Please  Enter the pin "%s" to pair Please Select the Device to pair Please Select the Network to connect PostalCode REMOVE Remove "%s" network? Remove Device? S Y S T E M  G E S T U R E  S E T T I N G S SEARCH SET DEFAULT SIGNAL Sorry Wifi is OFF State Street&HouseNo Telephone User with this mail-id is already registered Verification W I F I  S E T T I N G S WIFI OFF WLAN HOTSPOTS Y E S YES Project-Id-Version: mildenhall-settings 2.02.06
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-02-18 12:32+0530
PO-Revision-Date: 2013-02-18 12:33+0530
Last-Translator: x <x@in.com>
Language-Team: German
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 '%s' wird als Standard-Benutzer eingestellt werden A P P S T O R E E I N S T E L L U N G E N KONTO HINZUFUGEN Authentifizierung fehlgeschlagen B L U E T O O T H - E I N S T E L L U N G E N BT ist AUS Bluetooth Adapter am Bluetooth Adapter entfernt S C H L I E S S E N SCHLIESSEN CONNECT Card Namen CardNumber Card Typ Stadt Country Erstellen eines neuen Benutzerkontos ACCOUNT LOSCHEN AUS DER LISTE LOSCHEN TRENNEN Konto loschen '%s' Benutzer-ID Device "%s" getrennt Device "%s" verbunden Gerate-Verbindung fehlgeschlagen Deaktivieren Multi Touch Lassen Sie keine leeren Eintrag Sie wollen ein Gerat Scan? Wollen Sie ein Netz suchen? E-mail Id ENGLISCH Gultig Monat Gultig Jahr Vorname DEUTSCH Falsches Passwort S P R A C H E I N S T E L L U N G E N Nachname Multi-Touch mit 2 Punkten Multi-Touch mit 3 Punkten N O Netzwerk "%s" bereits angeschlossen Netzwerk "%s" getrennt Netzwerk "%s" fehlgeschlagen Netzwerk "%s" verbunden Netzwerk "%s" ist bereit, aber nicht angeschlossen Newsletter Nicht in der Lage, Paar! Bitte uberprufen Sie das Gerat NEUES GERAT KOPPELN Bitte geben Sie den Stift "%s" zu paaren Bitte Wahlen Sie das Gerat Paar Bitte wahlen Sie den Netzwerk zu verbinden Postalcode ENTFERNEN Remove "%s" Netzwerk? Gerat entfernen? S Y S T E M  G E S T E N  E I N S T E L L U N G E N SUCHE SET DEFAULT SIGNAL Leider Wifi ist AUS Zustand Street&HouseNo Telephone Benutzer mit dieser E-Mail-ID ist bereits registriert Uberprufung W I F I - E I N S T E L L U N G E N WIFI OFF WLAN-Hotspots J A JA 