/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <mildenhall-settings-system.h>

G_DEFINE_INTERFACE (MildenhallSettingsSystem, mildenhall_settings_system, 0)

static void
mildenhall_settings_system_default_init (MildenhallSettingsSystemInterface *iface)
{
  iface->get_with_default_view = NULL;
  iface->get_by_view_name = NULL;
  iface->show_view = NULL;
  iface->handle_back_press = NULL;
}

ClutterActor *
mildenhall_settings_system_get_with_default_view (MildenhallSettingsSystem *self)
{
  MildenhallSettingsSystemInterface *iface;

  g_return_val_if_fail (MILDENHALL_SETTINGS_IS_SYSTEM(self), NULL);

  iface = MILDENHALL_SETTINGS_SYSTEM_GET_IFACE(self);
  g_return_val_if_fail (iface->get_with_default_view != NULL, NULL);
  return (iface->get_with_default_view (self));
}

ClutterActor *
mildenhall_settings_system_get_by_view_name (MildenhallSettingsSystem *self,
					     const gchar *view_name)
{
  MildenhallSettingsSystemInterface *iface;

  g_return_val_if_fail (MILDENHALL_SETTINGS_IS_SYSTEM(self), NULL);

  iface = MILDENHALL_SETTINGS_SYSTEM_GET_IFACE(self);
  g_return_val_if_fail (iface->get_by_view_name != NULL, NULL);
  return (iface->get_by_view_name (self, view_name));
}

void
mildenhall_settings_system_show_view (MildenhallSettingsSystem *self,
				      const gchar *view_name)
{
  MildenhallSettingsSystemInterface *iface;

  g_return_if_fail (MILDENHALL_SETTINGS_IS_SYSTEM(self));

  iface = MILDENHALL_SETTINGS_SYSTEM_GET_IFACE(self);
  g_return_if_fail (iface->show_view != NULL);
  iface->show_view (self, view_name);
}

gboolean
mildenhall_settings_system_handle_back_press (MildenhallSettingsSystem *self)
{
  MildenhallSettingsSystemInterface *iface;

  g_return_val_if_fail (MILDENHALL_SETTINGS_IS_SYSTEM(self), FALSE);

  iface = MILDENHALL_SETTINGS_SYSTEM_GET_IFACE(self);
  g_return_val_if_fail (iface->handle_back_press != NULL, FALSE);
  return (iface->handle_back_press (self));
}
