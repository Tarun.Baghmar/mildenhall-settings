/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-settings-controller.h"

#include "mildenhall-settings-client-handler.h"
#include "mildenhall-settings-view.h"
#include "mildenhall-settings-wifi/mildenhall-settings-wifi.h"
#include "mildenhall-settings.h"

#define MAIN_CONTAINER_CLIP_Y_POS 480
#define MAIN_CONTAINER_CLIP_X_POS 792.0
#define SLIDE_IN_ANIMATION_DELAY 200

typedef GType (*GTypeGetFunc) (void);

typedef struct
{
  const gchar *name;
  const gchar *icon;
  GTypeGetFunc get_type_func;
} MildenhallSettingsTypes;

MildenhallSettingsTypes mildenhall_settings_types[] = {
  { "WIFI SETTINGS", "settings_icon_wifi_on_AC.png", mildenhall_settings_wifi_get_type },
};

typedef struct _MildenhallSettingsController MildenhallSettingsController;

typedef MildenhallSettingsSystem *mildenhall_settings_controller_get_settings_system (gchar *settings_name);

typedef enum _MildenhallSettingsControllerStates {
  MILDENHALL_SETTINGS_STATE_DEFAULT,
  MILDENHALL_SETTINGS_STATE_LAUNCHING_IN_PROGRESS,
  MILDENHALL_SETTINGS_STATE_LAUNCHED_AND_ACTIVE,
  MILDENHALL_SETTINGS_STATE_CLOSURE_IN_PROGRESS,
  MILDENHALL_SETTINGS_STATE_TERMINATING_PARENT_PROCESS,
  MILDENHALL_SETTINGS_STATE_DIRECT_LAUNCH
} MildenhallSettingsControllerStates;

enum
{
  MILDENHALL_SETTINGS_CONTROLLER_APP_NAME = 1,
  MILDENHALL_SETTINGS_CONTROLLER_LAUNCH_SETTINGS,
  MILDENHALL_SETTINGS_CONTROLLER_VIEW_OBJECT,
  MILDENHALL_SETTINGS_CONTROLLER_VIEW_NAME,
  MILDENHALL_SETTINGS_CONTROLLER_ENUM_LAST
};

typedef struct _MildenhallSettingsDetail
{
  const gchar *icon;
  gchar *name;
  GTypeGetFunc get_type_func;
} MildenhallSettingsDetail;

#define ANIMATION_EASE_DURATION 900
#define MAIN_VIEW_IN_X_POS 0
#define MAIN_VIEW_OUT_X_POS -726
#define SECOND_VIEW_IN_X_POS 67
#define SECOND_VIEW_OUT_X_POS 790
#define SET_ROLLER_FOCUS_TIME 700

struct _MildenhallSettingsController
{
  GObject parent;
  MildenhallSettingsView *setting_view; /*owned */
  gchar *launch_settings_name;          /* owned */
  gchar *settings_view_name;            /* owned */
  gchar *app_name;                      /* owned */
  gboolean maximized;
  gboolean progress;
  guint main_roller_focused_row;
  guint main_container_source_id;
  MildenhallSettingsControllerStates state_mac;
  GSList *list_of_settings_detail; /* owned */
  gboolean is_back_handled;
  MildenhallSettingsClientHandler *client_handler; /* owned */
  MildenhallSettingsSystem *sub_settings_object;   /* owned */
  ClutterActor *sub_settings_actor;                /*unowned*/
};

G_DEFINE_TYPE (MildenhallSettingsController, mildenhall_settings_controller, G_TYPE_OBJECT)

static void
mildenhall_settings_controller_update_main_roller (MildenhallSettingsController *self);
static gboolean
mildenhall_settings_controller_set_main_roller_to_focus (gpointer user_data);
static void
mildenhall_settings_controller_get_app_settings_db_list_cb (GObject *client_handler,
                                                            GParamSpec *pspec,
                                                            gpointer data);
static void
mildenhall_settings_controller_start_slide_out_animation (MildenhallSettingsController *self);
static guint
mildenhall_settings_controller_settings_find_match_in_model (ThornburyModel *model,
                                                             const gchar *string_to_search,
                                                             guint column_num);
static void
mildenhall_settings_controller_roller_item_activated_cb (MildenhallRollerContainer *roller,
                                                         guint row,
                                                         gpointer data);
static void
mildenhall_settings_controller_settings_list_free (GSList *list_of_settings_detail);

static void
mildenhall_settings_controller_back_press_status (CanterburyHardKeys *object,
                                                  const gchar *arg_app_name,
                                                  gpointer user_data);

static void
mildenhall_settings_controller_dispose (GObject *object)
{
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (object);

  g_clear_object (&self->client_handler);
  g_clear_object (&self->setting_view);
  mildenhall_settings_controller_settings_list_free (self->list_of_settings_detail);
  g_clear_object (&self->sub_settings_object);
  G_OBJECT_CLASS (mildenhall_settings_controller_parent_class)
      ->dispose (
          object);
}

static void
mildenhall_settings_controller_finalize (GObject *object)
{
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (object);

  g_clear_pointer (&self->app_name, g_free);
  g_clear_pointer (&self->launch_settings_name, g_free);
  g_clear_pointer (&self->settings_view_name, g_free);
  G_OBJECT_CLASS (mildenhall_settings_controller_parent_class)
      ->finalize (
          object);
}

static void
mildenhall_settings_controller_get_property (GObject *object,
                                             guint property_id,
                                             GValue *value,
                                             GParamSpec *pspec)
{
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_CONTROLLER_APP_NAME:
      g_value_set_string (value, self->app_name);
      break;

    case MILDENHALL_SETTINGS_CONTROLLER_VIEW_OBJECT:
      g_value_set_object (value, self->setting_view);
      break;

    case MILDENHALL_SETTINGS_CONTROLLER_LAUNCH_SETTINGS:
      g_value_set_string (value, self->launch_settings_name);
      break;

    case MILDENHALL_SETTINGS_CONTROLLER_VIEW_NAME:
      g_value_set_string (value, self->settings_view_name);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_controller_set_property (GObject *object,
                                             guint property_id,
                                             const GValue *value,
                                             GParamSpec *pspec)
{
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_CONTROLLER_APP_NAME:
      self->app_name = g_value_dup_string (value);
      break;

    case MILDENHALL_SETTINGS_CONTROLLER_VIEW_OBJECT:
      self->setting_view = g_value_dup_object (value);
      break;

    case MILDENHALL_SETTINGS_CONTROLLER_LAUNCH_SETTINGS:
      self->launch_settings_name = g_value_dup_string (value);
      break;

    case MILDENHALL_SETTINGS_CONTROLLER_VIEW_NAME:
      self->settings_view_name = g_value_dup_string (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_controller_detail_free (MildenhallSettingsDetail *detail)
{
  g_free (detail->name);
  g_free (detail);
}

static void
mildenhall_settings_controller_settings_list_free (GSList *list_of_settings_detail)
{
  g_slist_free_full (list_of_settings_detail, (GDestroyNotify) mildenhall_settings_controller_detail_free);
}

static gchar *
mildenhall_settings_controller_convert_to_uppercase_with_spaces (const gchar *label)
{
  gchar old_string[50];
  gchar spaced_string[50];
  gchar *result_string = NULL;
  guint i = 0;
  guint j = 0;

  strcpy (old_string, label);

  for (; i < strlen (old_string); i++)
    {
      spaced_string[j] = old_string[i];
      j++;
      spaced_string[j] = ' ';
      j++;
    }
  spaced_string[j] = '\0';

  result_string = g_ascii_strup (spaced_string, strlen (spaced_string));
  DEBUG ("Converted String = %s", result_string);
  return result_string;
}

static void
mildenhall_settings_controller_constrcuted (GObject *object)
{
  MildenhallSettingsDetail *data;
  guint i;
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (object);
  self->client_handler = mildenhall_settings_client_handler_new (self->app_name);

  if (self->launch_settings_name != NULL)
    {
      clutter_actor_set_x (self->setting_view->main_container,
                           MAIN_VIEW_OUT_X_POS);
      clutter_actor_set_x (self->setting_view->second_container,
                           SECOND_VIEW_IN_X_POS);
      self->state_mac = MILDENHALL_SETTINGS_STATE_DIRECT_LAUNCH;
      self->maximized = FALSE;
      self->progress = FALSE;
    }
  else
    {
      self->maximized = TRUE;
      self->progress = FALSE;

      for (i = 0; i < G_N_ELEMENTS (mildenhall_settings_types); i++)
        {
          DEBUG ("name = %s and icon = %s", mildenhall_settings_types[i].name, mildenhall_settings_types[i].icon);
          data = g_new0 (MildenhallSettingsDetail, 1);
          data->icon = mildenhall_settings_types[i].icon;
          data->name = mildenhall_settings_controller_convert_to_uppercase_with_spaces (mildenhall_settings_types[i].name);
          data->get_type_func = (GTypeGetFunc) mildenhall_settings_types[i].get_type_func;
          self->list_of_settings_detail = g_slist_append (self->list_of_settings_detail, data);
        }
      mildenhall_settings_controller_update_main_roller (self);

      g_signal_connect (G_OBJECT (self->setting_view->main_roller),
                        "roller-item-activated",
                        G_CALLBACK (mildenhall_settings_controller_roller_item_activated_cb),
                        self);

      g_timeout_add (SET_ROLLER_FOCUS_TIME,
                     mildenhall_settings_controller_set_main_roller_to_focus,
                     self);
    }

  g_signal_connect (self->client_handler, "notify::app-database-list",
                    G_CALLBACK (mildenhall_settings_controller_get_app_settings_db_list_cb),
                    self);

  g_signal_connect (self->client_handler,
                    "response-back-press",
                    G_CALLBACK (mildenhall_settings_controller_back_press_status),
                    self);
}

static void
mildenhall_settings_controller_class_init (MildenhallSettingsControllerClass *klass)
{
  GParamSpec *pspec = NULL;
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = mildenhall_settings_controller_get_property;
  object_class->set_property = mildenhall_settings_controller_set_property;
  object_class->dispose = mildenhall_settings_controller_dispose;
  object_class->finalize = mildenhall_settings_controller_finalize;
  object_class->constructed = mildenhall_settings_controller_constrcuted;

  pspec = g_param_spec_object ("view-object", "view-object", "Settings View object", MILDENHALL_SETTINGS_TYPE_VIEW,
                               (G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_CONTROLLER_VIEW_OBJECT, pspec);

  pspec = g_param_spec_string ("app-name", "app-name", "Settings app name", NULL,
                               (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_CONTROLLER_APP_NAME, pspec);

  pspec = g_param_spec_string ("launch-settings-name", "launch-settings-name", "settings name", NULL,
                               (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_CONTROLLER_LAUNCH_SETTINGS, pspec);

  pspec = g_param_spec_string ("view-name", "view-name", "Settings view name", NULL,
                               (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_CONTROLLER_VIEW_NAME, pspec);
}

static void
mildenhall_settings_controller_init (MildenhallSettingsController *self)
{
  self->sub_settings_actor = NULL;
}

MildenhallSettingsController *
mildenhall_settings_controller_new (GObject *view_object,
                                    const gchar *app_name,
                                    const gchar *launch_settings_name,
                                    const gchar *view_name)
{
  return g_object_new (MILDENHALL_SETTINGS_TYPE_CONTROLLER,
                       "view-object", view_object,
                       "app-name", app_name,
                       "launch-settings-name", launch_settings_name,
                       "view-name", view_name,
                       NULL);
}

static gboolean
mildenhall_settings_controller_handle_slide_in_progress_cb (ClutterTimeline *timeline,
                                                            gint msecs,
                                                            gpointer user_data)
{
  gfloat x, y;
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (user_data);
  MildenhallSettingsView *settings_view = MILDENHALL_SETTINGS_VIEW (self->setting_view);

  clutter_actor_get_transformed_position (settings_view->main_container, &x,
                                          &y);
  /*main container holding the setting names in the roller item is clip to the full size using the
   *below clutter api*/
  clutter_actor_set_clip (settings_view->main_container, (x * -1), 0,
                          (MAIN_CONTAINER_CLIP_X_POS + x), MAIN_CONTAINER_CLIP_Y_POS);
  return TRUE;
}

static void
mildenhall_settings_controller_get_app_settings_db_list_cb (GObject *client_handler,
                                                            GParamSpec *pspec,
                                                            gpointer user_data)
{
  GSList *list_of_app_data_recieved;
  MildenhallSettingsAppListData *data;
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (user_data);
  MildenhallSettingsView *settings_view = MILDENHALL_SETTINGS_VIEW (self->setting_view);

  g_object_get (self->client_handler, "app-database-list", &list_of_app_data_recieved, NULL);

  for (; list_of_app_data_recieved; list_of_app_data_recieved = list_of_app_data_recieved->next)
    {
      data = (MildenhallSettingsAppListData *) list_of_app_data_recieved->data;
      thornbury_model_append (settings_view->main_roller_model,
                              ROLLER_COLUMN_MODULE_NAME,
                              data->settings_path,
                              ROLLER_COLUMN_LEFT_ICON,
                              data->icon,
                              ROLLER_COLUMN_RIGHT_ICON,
                              data->icon,
                              ROLLER_COLUMN_LABEL,
                              data->label,
                              ROLLER_COLUMN_APP_SETTING, TRUE, -1);
      DEBUG ("recieved icon = %s, recieved label=%s, recieved settings path = %s",
             data->icon, data->label, data->settings_path);
    }

  mildenhall_roller_container_refresh (
      MILDENHALL_ROLLER_CONTAINER (settings_view->main_roller));
}

static gboolean
mildenhall_settings_controller_set_main_roller_to_focus (gpointer user_data)
{
  gint in_row_to_focus;
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (user_data);
  MildenhallSettingsView *settings_view = MILDENHALL_SETTINGS_VIEW (self->setting_view);

  if (self->launch_settings_name != NULL)
    {
      in_row_to_focus =
          mildenhall_settings_controller_settings_find_match_in_model (
              settings_view->main_roller_model, self->launch_settings_name,
              ROLLER_COLUMN_LABEL);

      if (in_row_to_focus != -1)
        {
          mildenhall_roller_container_set_focused_row (
              MILDENHALL_ROLLER_CONTAINER (settings_view->main_roller),
              in_row_to_focus, TRUE);
        }
    }
  else
    {
      mildenhall_roller_container_set_focused_row (
          MILDENHALL_ROLLER_CONTAINER (settings_view->main_roller), 0, TRUE);
    }
  return FALSE;
}

static void
mildenhall_settings_controller_update_main_roller (MildenhallSettingsController *self)
{
  GSList *iter;
  MildenhallSettingsDetail *data;
  gchar *setting_icon = NULL;
  MildenhallSettingsView *settings_view = MILDENHALL_SETTINGS_VIEW (self->setting_view);

  for (iter = self->list_of_settings_detail; iter; iter = iter->next)
    {
      data = (MildenhallSettingsDetail *) iter->data;
      setting_icon = g_build_filename (PKGDATADIR, data->icon, NULL);
      thornbury_model_append (settings_view->main_roller_model,
                              ROLLER_COLUMN_GET_TYPE_FUNC, data->get_type_func,
                              ROLLER_COLUMN_LEFT_ICON,
                              setting_icon,
                              ROLLER_COLUMN_RIGHT_ICON,
                              setting_icon,
                              ROLLER_COLUMN_LABEL, data->name,
                              ROLLER_COLUMN_APP_SETTING, FALSE, -1);
      g_clear_pointer (&setting_icon, g_free);
    }
}

static guint
mildenhall_settings_controller_settings_find_match_in_model (ThornburyModel *model,
                                                             const gchar *string_to_search,
                                                             guint column_num)
{
  guint row_num = -1;
  guint total_rows;
  guint row_count;
  ThornburyModelIter *iter;
  const gchar *string_in_model;
  GValue value = {
    0,
  };

  g_return_val_if_fail (THORNBURY_IS_MODEL (model), row_num);

  total_rows = thornbury_model_get_n_rows (model);

  for (row_count = 0; row_count < total_rows; row_count++)
    {
      iter = thornbury_model_get_iter_at_row (model, row_count);
      if (iter != NULL)
        {
          thornbury_model_iter_get_value (iter, column_num, &value);
          string_in_model = g_value_get_string (&value);

          if (!g_strcmp0 (string_in_model, string_to_search))
            {
              g_value_unset (&value);
              g_object_unref (iter);
              row_num = row_count;
              break;
            }
          g_value_unset (&value);
        }
      g_object_unref (iter);
    }
  return row_num;
}

static void
mildenhall_settings_controller_create_settings_object (MildenhallSettingsController *self,
                                                       GTypeGetFunc get_type_func)
{
  MildenhallSettingsView *settings_view = MILDENHALL_SETTINGS_VIEW (self->setting_view);
  GType settings_type = get_type_func ();

  if (settings_type != G_TYPE_NONE && settings_type != G_TYPE_INVALID)
    {
      self->sub_settings_object = g_object_new (settings_type, NULL);
      self->sub_settings_actor = mildenhall_settings_system_get_with_default_view (self->sub_settings_object);
      self->is_back_handled = mildenhall_settings_system_handle_back_press (self->sub_settings_object);
      clutter_actor_add_child (CLUTTER_ACTOR (settings_view->second_container),
                               self->sub_settings_actor);
    }
}

static void
mildenhall_settings_controller_launch_new_settings_app_by_quick_menu_entry (gpointer data,
                                                                            gchar *settings_name,
                                                                            GTypeGetFunc settings_type)
{
  MildenhallSettingsController *self = MILDENHALL_SETTINGS_CONTROLLER (data);

  DEBUG ("settings_name = %s ", settings_name);

  if (self->sub_settings_object == NULL)
    {
      mildenhall_settings_controller_create_settings_object (self,
                                                             settings_type);
    }
}

static void
mildenhall_settings_controller_launch_new_settings_app (ThornburyModel *model,
                                                        guint row,
                                                        gpointer data)
{
  gboolean is_app_setting;
  gchar *settings_name = NULL;
  GTypeGetFunc settings_type = NULL;
  ThornburyModelIter *iter = thornbury_model_get_iter_at_row (model, row);

  thornbury_model_iter_get (iter, ROLLER_COLUMN_LABEL, &settings_name, -1);
  thornbury_model_iter_get (iter, ROLLER_COLUMN_APP_SETTING, &is_app_setting,
                            -1);
  thornbury_model_iter_get (iter, ROLLER_COLUMN_GET_TYPE_FUNC, &settings_type,
                            -1);

  if (is_app_setting == FALSE)
    {
      mildenhall_settings_controller_launch_new_settings_app_by_quick_menu_entry (
          data, settings_name, settings_type);
    }
  else
    {
      //FIXME: launching of applications settings have to be handled here
    }
  g_clear_pointer (&settings_name, g_free);
}

static gboolean
mildenhall_settings_controller_wait_for_a_while (gpointer user_data)
{
  ThornburyModel *model;
  MildenhallSettingsController *self =
      (MildenhallSettingsController *) user_data;
  MildenhallSettingsView *settings_view =
      MILDENHALL_SETTINGS_VIEW (self->setting_view);

  model = mildenhall_roller_container_get_model (
      MILDENHALL_ROLLER_CONTAINER (settings_view->main_roller));
  mildenhall_settings_controller_launch_new_settings_app (
      model, self->main_roller_focused_row, user_data);
  self->maximized = FALSE;
  self->progress = FALSE;
  self->state_mac = MILDENHALL_SETTINGS_STATE_LAUNCHED_AND_ACTIVE;
  return FALSE;
}

static void
mildenhall_settings_controller_on_settings_min_effect_complete (ClutterActor *actor,
                                                                gpointer user_data)
{
  MildenhallSettingsController *self =
      (MildenhallSettingsController *) user_data;
  MildenhallSettingsView *settings_view =
      MILDENHALL_SETTINGS_VIEW (self->setting_view);

  mildenhall_roller_container_set_focused_row (
      MILDENHALL_ROLLER_CONTAINER (settings_view->main_roller),
      self->main_roller_focused_row, FALSE);
  g_signal_handler_disconnect (actor, self->main_container_source_id);
  /* timeout is added to have to delay before launching the settings, to see the animation completely*/
  g_timeout_add (SLIDE_IN_ANIMATION_DELAY, mildenhall_settings_controller_wait_for_a_while,
                 user_data);
}

static void
mildenhall_settings_controller_start_slide_in_animation (MildenhallSettingsController *self,
                                                         guint row)
{
  ClutterTimeline *timeline;
  MildenhallSettingsView *settings_view =
      MILDENHALL_SETTINGS_VIEW (self->setting_view);
  /* swipe out the main settings roller  */
  self->progress = TRUE;
  self->state_mac = MILDENHALL_SETTINGS_STATE_LAUNCHING_IN_PROGRESS;
  clutter_actor_save_easing_state (settings_view->main_container);
  clutter_actor_set_easing_mode (settings_view->main_container,
                                 CLUTTER_EASE_OUT_CUBIC);
  clutter_actor_set_easing_duration (settings_view->main_container,
                                     ANIMATION_EASE_DURATION);
  clutter_actor_set_x (settings_view->main_container, MAIN_VIEW_OUT_X_POS);
  clutter_actor_restore_easing_state (settings_view->main_container);

  timeline = clutter_timeline_new (900);

  clutter_timeline_start (timeline);
  g_signal_connect (timeline,
                    "new-frame",
                    G_CALLBACK (mildenhall_settings_controller_handle_slide_in_progress_cb),
                    self);
  clutter_actor_save_easing_state (settings_view->second_container);
  clutter_actor_set_easing_mode (settings_view->second_container,
                                 CLUTTER_EASE_OUT_CUBIC);
  clutter_actor_set_easing_duration (settings_view->second_container,
                                     ANIMATION_EASE_DURATION);
  clutter_actor_set_x (settings_view->second_container, SECOND_VIEW_IN_X_POS);
  clutter_actor_restore_easing_state (settings_view->second_container);

  self->main_roller_focused_row = row;
  self->main_container_source_id = g_signal_connect (settings_view->main_container,
                                                     "transitions-completed",
                                                     G_CALLBACK (mildenhall_settings_controller_on_settings_min_effect_complete),
                                                     self);
}

static void
mildenhall_settings_controller_destroy_settings_app (ThornburyModel *model,
                                                     guint row,
                                                     gpointer data)
{
  MildenhallSettingsController *self = (MildenhallSettingsController *) data;

  g_clear_object (&self->sub_settings_object);
}

static void
mildenhall_settings_controller_roller_item_activated_cb (MildenhallRollerContainer *roller,
                                                         guint row,
                                                         gpointer data)
{
  ThornburyModel *model;
  MildenhallSettingsController *self = (MildenhallSettingsController *) data;

  model = mildenhall_roller_container_get_model (roller);

  if (self->maximized)
    {
      /* Ignore Multiple roller item activation call */
      if (MILDENHALL_SETTINGS_STATE_DEFAULT == self->state_mac)
        {
          mildenhall_settings_controller_start_slide_in_animation (data, row);
        }
      else
        g_message (" Ignoring Multiple launch request \n");
    }
  else
    {
      if (self->main_roller_focused_row != row)
        {
          mildenhall_settings_controller_destroy_settings_app (
              model, self->main_roller_focused_row, data);
          self->main_roller_focused_row = row;

          /* In case of Direct Launch */
          if (MILDENHALL_SETTINGS_STATE_LAUNCHED_AND_ACTIVE != self->state_mac)
            {
              self->state_mac = MILDENHALL_SETTINGS_STATE_LAUNCHED_AND_ACTIVE;
            }

          mildenhall_settings_controller_launch_new_settings_app (model, row,
                                                                  data);
        }
    }
}

static void
mildenhall_settings_controller_on_max_effect_complete (ClutterActor *actor,
                                                       gpointer user_data)
{
  ThornburyModel *model;
  MildenhallSettingsView *settings_view;
  MildenhallSettingsController *self;

  self = (MildenhallSettingsController *) user_data;
  settings_view = MILDENHALL_SETTINGS_VIEW (self->setting_view);

  self->maximized = TRUE;
  mildenhall_roller_container_set_focused_row (
      MILDENHALL_ROLLER_CONTAINER (settings_view->main_roller),
      self->main_roller_focused_row, FALSE);
  g_signal_handler_disconnect (actor, self->main_container_source_id);

  model = mildenhall_roller_container_get_model (
      MILDENHALL_ROLLER_CONTAINER (settings_view->main_roller));
  mildenhall_settings_controller_destroy_settings_app (
      model, self->main_roller_focused_row, user_data);
  self->progress = FALSE;
  self->state_mac = MILDENHALL_SETTINGS_STATE_DEFAULT;
}

static void
mildenhall_settings_controller_start_slide_out_animation (MildenhallSettingsController *self)
{

  MildenhallSettingsView *settings_view =
      MILDENHALL_SETTINGS_VIEW (self->setting_view);
  self->state_mac = MILDENHALL_SETTINGS_STATE_CLOSURE_IN_PROGRESS;

  /* swipe out the main settings roller  */
  clutter_actor_remove_clip (settings_view->main_container);
  clutter_actor_save_easing_state (settings_view->main_container);
  clutter_actor_set_easing_mode (settings_view->main_container,
                                 CLUTTER_EASE_OUT_CUBIC);
  clutter_actor_set_easing_duration (settings_view->main_container,
                                     ANIMATION_EASE_DURATION);
  clutter_actor_set_x (settings_view->main_container, MAIN_VIEW_IN_X_POS);
  clutter_actor_restore_easing_state (settings_view->main_container);

  clutter_actor_save_easing_state (settings_view->second_container);
  clutter_actor_set_easing_mode (settings_view->second_container,
                                 CLUTTER_EASE_OUT_CUBIC);
  clutter_actor_set_easing_duration (settings_view->second_container,
                                     ANIMATION_EASE_DURATION);
  clutter_actor_set_x (settings_view->second_container, SECOND_VIEW_OUT_X_POS);
  clutter_actor_restore_easing_state (settings_view->second_container);
  self->progress = TRUE;
  self->main_container_source_id = g_signal_connect (settings_view->main_container,
                                                     "transitions-completed",
                                                     G_CALLBACK (mildenhall_settings_controller_on_max_effect_complete),
                                                     self);
}

static void
mildenhall_settings_controller_back_press_status (CanterburyHardKeys *object,
                                                  const gchar *arg_app_name,
                                                  gpointer user_data)
{
  MildenhallSettingsController *self =
      (MildenhallSettingsController *) user_data;

  if (g_strcmp0 (arg_app_name, self->app_name) == FALSE)
    {
      if (self->progress == TRUE)
        {
          canterbury_hard_keys_call_back_press_consumed_sync (
              self->client_handler->hardkeys_mgr_proxy, self->app_name, TRUE,
              NULL,
              NULL);
        }
      else
        {
          if (self->maximized)
            {
              canterbury_hard_keys_call_back_press_consumed_sync (
                  self->client_handler->hardkeys_mgr_proxy, self->app_name, FALSE,
                  NULL,
                  NULL);
            }
          else
            {
              if (FALSE == self->is_back_handled)
                {
                  DEBUG ("State macro = %d", self->state_mac);
                  if (MILDENHALL_SETTINGS_STATE_DIRECT_LAUNCH != self->state_mac)
                    {
                      /* inform app-manager that back event is consumed */
                      canterbury_hard_keys_call_back_press_consumed_sync (
                          self->client_handler->hardkeys_mgr_proxy,
                          self->app_name, TRUE, NULL,
                          NULL);

                      mildenhall_settings_controller_start_slide_out_animation (
                          user_data);
                    }
                  else
                    {
                      /* Terminate the Process */
                      canterbury_hard_keys_call_back_press_consumed_sync (
                          self->client_handler->hardkeys_mgr_proxy,
                          self->app_name, FALSE, NULL,
                          NULL);
                    }
                }
              else
                {
                  /* Since back is handled by the sub-settings,
		   * inform app-manager that back event is consumed */
                  canterbury_hard_keys_call_back_press_consumed_sync (
                      self->client_handler->hardkeys_mgr_proxy, self->app_name,
                      TRUE, NULL,
                      NULL);
                }
            }
        }
    }
}
