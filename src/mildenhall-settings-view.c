/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-settings-view.h"

G_DEFINE_TYPE (MildenhallSettingsView, mildenhall_settings_view, G_TYPE_OBJECT)

static void
mildenhall_settings_view_dispose (GObject *object)
{
  MildenhallSettingsView *self = MILDENHALL_SETTINGS_VIEW (object);

  g_clear_object (&self->main_roller_model);
  g_clear_pointer (&self->main_window, clutter_actor_destroy);
  G_OBJECT_CLASS (mildenhall_settings_view_parent_class)->dispose (
      object);
}

static void
mildenhall_settings_view_class_init (MildenhallSettingsViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = mildenhall_settings_view_dispose;
}

static void
mildenhall_settings_view_create_main_roller_model (MildenhallSettingsView *self)
{
  self->main_roller_model = (ThornburyModel *) thornbury_list_model_new (ROLLER_COLUMN_LAST,
                                                                         G_TYPE_STRING, NULL,
                                                                         G_TYPE_STRING, NULL,
                                                                         G_TYPE_STRING, NULL,
                                                                         G_TYPE_STRING, NULL,
                                                                         G_TYPE_POINTER, NULL,
                                                                         G_TYPE_BOOLEAN, NULL,
                                                                         -1);
}

static ClutterActor *
mildenhall_settings_view_create_main_roller (ThornburyModel * model)
{
  ClutterActor *roller;
  ThornburyItemFactory *item_factory;
  GObject *object;
  gchar *main_roller_prop_file_path;

  main_roller_prop_file_path = g_strdup (PKGDATADIR"/settings_main_roller_prop.json");
  item_factory = thornbury_item_factory_generate_widget_with_props (MILDENHALL_TYPE_ROLLER_CONTAINER,
                                                                    main_roller_prop_file_path);
  g_object_get (item_factory, "object", &object, NULL);
  roller = CLUTTER_ACTOR (object);
  g_object_set (object, "item-type", TYPE_ICON_LABEL_ICON_ITEM, "model", model, NULL);

  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller),
                                             "label", ROLLER_COLUMN_LABEL);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller),
                                             "name", ROLLER_COLUMN_MODULE_NAME);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller),
                                             "icon", ROLLER_COLUMN_LEFT_ICON);
  mildenhall_roller_container_add_attribute (MILDENHALL_ROLLER_CONTAINER (roller),
                                             "icon2", ROLLER_COLUMN_RIGHT_ICON);

  g_clear_pointer (&main_roller_prop_file_path, g_free);
  return roller;
}

static void
mildenhall_settings_view_init (MildenhallSettingsView *self)
{
  ClutterActor *overlay;
  gchar *background = NULL;

  self->main_window = clutter_actor_new ();
  self->main_container = clutter_actor_new ();
  self->second_container = clutter_actor_new ();
  background = g_strconcat (PKGDATADIR, "/settings-bkg.png", NULL);
  self->settings_bkg = thornbury_ui_texture_create_new (background, 799, 480,
							FALSE, TRUE);
  mildenhall_settings_view_create_main_roller_model (self);
  self->main_roller = mildenhall_settings_view_create_main_roller (
      self->main_roller_model);

  clutter_actor_set_position (self->main_container, 0, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (self->main_container),
			   self->main_roller);
  clutter_actor_add_child (CLUTTER_ACTOR (self->second_container),
			   self->settings_bkg);
  clutter_actor_set_position (self->settings_bkg, 0, 5);
  clutter_actor_add_child (CLUTTER_ACTOR (self->main_window),
			   self->main_container);
  clutter_actor_add_child (CLUTTER_ACTOR (self->main_window),
			   self->second_container);
  overlay = mildenhall_ui_utils_create_application_overlay ();

  clutter_actor_add_child (CLUTTER_ACTOR (self->main_window), overlay);
  clutter_actor_set_position (self->second_container, 792, 0);
  g_clear_pointer (&background, g_free);
}

MildenhallSettingsView *
mildenhall_settings_view_new (void)
{
  return g_object_new (MILDENHALL_SETTINGS_TYPE_VIEW, NULL);
}
