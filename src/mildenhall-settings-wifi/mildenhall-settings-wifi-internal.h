/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#ifndef MILDENHALL_SETTINGS_WIFI_INTERNAL_H_
#define MILDENHALL_SETTINGS_WIFI_INTERNAL_H_

#define WIFI_POWERED_STATUS "WifiPoweredStatus"
#define WIFI_OFF_AC PKGDATADIR "/icon_wifi_off_AC.png"
#define WIFI_ON_AC PKGDATADIR "/icon_wifi_on_AC.png"
#define WIFI_ON "WIFI ON"
#define WIFI_OFF "WIFI OFF"

#endif /* MILDENHALL_SETTINGS_WIFI_INTERNAL_H_ */
