/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */
#include "mildenhall-settings-wifi-backend.h"
#include "connman_technology_gdbus_fi.h"
#include "mildenhall-settings.h"

#define ADAPTER_NOT_ADDED "Adapter is not available"
#define FAILED "Operation Failed"

typedef struct _MildenhallSettingsWifiBackend
{
  GObject parent;

  gulong connman_handler_id;                    /* owned */
  NetConnmanTechnology *net_connman_tech_proxy; /* owned */
  gboolean wifi_status;                         /* unowned */
  GCancellable *cancellable;                    /* owned */
} MildenhallSettingsWifiBackend;

G_DEFINE_TYPE (MildenhallSettingsWifiBackend, mildenhall_settings_wifi_backend, G_TYPE_OBJECT)

enum MildenhallSettingsWifiBackendProperties
{
  MILDENHALL_SETTINGS_WIFI_BACKEND_ENUM_FIRST,
  MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS,
  MILDENHALL_SETTINGS_WIFI_BACKEND_ENUM_LAST
};

enum
{
  WIFI_SETTINGS_START,
  WIFI_SETTINGS_ERROR_SIGNAL,
  WIFI_SETTINGS_LAST_SIGNAL
};

static GParamSpec *obj_properties[MILDENHALL_SETTINGS_WIFI_BACKEND_ENUM_LAST] =
    {
      NULL,
    };

static guint32 wifi_backend_signals[WIFI_SETTINGS_LAST_SIGNAL] =
    {
      0,
    };

static void
mildenhall_settings_wifi_backend_dispose (GObject *object)
{
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (object);
  g_cancellable_cancel (wifi_backend->cancellable);
  g_clear_object (&wifi_backend->cancellable);

  g_clear_object (&wifi_backend->net_connman_tech_proxy);
  if (wifi_backend->connman_handler_id)
    {
      g_bus_unwatch_name (wifi_backend->connman_handler_id);
      wifi_backend->connman_handler_id = 0;
    }
  G_OBJECT_CLASS (mildenhall_settings_wifi_backend_parent_class)
      ->dispose (object);
}

/*  Description  :  Filter for Powered property  */
static gboolean
filter_for_powered_property (GVariant *technology_list)
{
  GVariantIter *iter;
  GVariant *value;
  gchar *key;
  gboolean status = FALSE;
  DEBUG ("entered");
  g_variant_get (technology_list, "(a{sv})", &iter);
  while (g_variant_iter_next (iter, "{&sv}", &key, &value))
    {
      if (!g_strcmp0 (key, "Powered"))
        {
          status = g_variant_get_boolean (value);
          g_clear_pointer (&value, g_variant_unref);
          break;
        }
      g_clear_pointer (&value, g_variant_unref);
    }
  g_variant_iter_free (iter);
  return status;
}

static void
wifi_net_connman_technology_call_get_properties_clb (GObject *source,
                                                     GAsyncResult *res,
                                                     gpointer user_data)
{
  GError *error = NULL;
  GVariant *returned_technology_list = NULL;
  GVariant *technology_list = NULL;
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (user_data);

  net_connman_technology_call_get_properties_finish (
      (NetConnmanTechnology *) source, &returned_technology_list, res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      wifi_backend->wifi_status = FALSE;
      g_signal_emit (wifi_backend,
                     wifi_backend_signals[WIFI_SETTINGS_ERROR_SIGNAL], 0,
                     ADAPTER_NOT_ADDED);
      g_object_notify_by_pspec (
          G_OBJECT (wifi_backend),
          obj_properties[MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS]);
      g_clear_error (&error);
    }
  else
    {
      technology_list = g_variant_new ("(@a{sv})", returned_technology_list);
      wifi_backend->wifi_status = filter_for_powered_property (technology_list);
      g_object_notify_by_pspec (
          G_OBJECT (wifi_backend),
          obj_properties[MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS]);
      g_clear_pointer (&technology_list, g_variant_unref);
    }
}

static void
mildenhall_settings_wifi_backend_set_wifi_status_clb (GObject *source,
                                                      GAsyncResult *res,
                                                      gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (user_data);

  net_connman_technology_call_set_property_finish (
      (NetConnmanTechnology *) source, res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      g_signal_emit (wifi_backend,
                     wifi_backend_signals[WIFI_SETTINGS_ERROR_SIGNAL], 0,
                     FAILED);
      g_clear_error (&error);
    }
}

void
mildenhall_settings_wifi_backend_set_wifi_status (MildenhallSettingsWifiBackend *wifi_backend,
                                                  gboolean toggled_wifi_status)
{
  GVariant *flag;
  flag = g_variant_new ("v", g_variant_new_boolean (toggled_wifi_status));
  net_connman_technology_call_set_property (
      wifi_backend->net_connman_tech_proxy, "Powered", flag, wifi_backend->cancellable,
      mildenhall_settings_wifi_backend_set_wifi_status_clb, wifi_backend);
}

/*  Description  :  Connman property changed callback  */
static void
wifi_net_connman_tech_property_changed (NetConnmanTechnology *object,
                                        const gchar *property,
                                        GVariant *value,
                                        gpointer user_data)
{
  GVariant *gvariant_temp_value;
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (user_data);
  DEBUG ("entered");

  if (g_strcmp0 ("Powered", property) == 0)
    {
      gvariant_temp_value = g_variant_get_variant (value);
      wifi_backend->wifi_status = g_variant_get_boolean (gvariant_temp_value);
      g_object_notify_by_pspec (
          G_OBJECT (wifi_backend),
          obj_properties[MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS]);
      g_clear_pointer (&gvariant_temp_value, g_variant_unref);
    }
}

/*  Description  :  Connman technology proxy created callback  */
static void
wifi_net_connman_technology_proxy_created_clb (GObject *source,
                                               GAsyncResult *res,
                                               gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (user_data);

  DEBUG ("entered");
  wifi_backend->net_connman_tech_proxy = net_connman_technology_proxy_new_finish (res, &error);
  if (error)
    {
      WARNING ("%s", error->message);
      g_clear_error (&error);
    }
  else
    {
      g_signal_connect (wifi_backend->net_connman_tech_proxy,
                        "property-changed",
                        G_CALLBACK (wifi_net_connman_tech_property_changed),
                        user_data);
      net_connman_technology_call_get_properties (
          wifi_backend->net_connman_tech_proxy, wifi_backend->cancellable,
          wifi_net_connman_technology_call_get_properties_clb, user_data);
    }
}

static void
wifi_net_connman_name_appeared (GDBusConnection *connection,
                                const gchar *name,
                                const gchar *name_owner,
                                gpointer user_data)

{
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (user_data);

  DEBUG ("entered");
  net_connman_technology_proxy_new (connection,
                                    G_DBUS_PROXY_FLAGS_NONE, "net.connman",
                                    "/net/connman/technology/wifi", wifi_backend->cancellable,
                                    wifi_net_connman_technology_proxy_created_clb,
                                    user_data);
}
static void
wifi_net_connman_name_vanished (GDBusConnection *connection,
                                const gchar *name,
                                gpointer user_data)
{
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (user_data);

  DEBUG ("entered");
  g_clear_object (&wifi_backend->net_connman_tech_proxy);
}

static void
mildenhall_settings_wifi_get_wifi_status_property (GObject *object,
                                                   guint property_id,
                                                   GValue *value,
                                                   GParamSpec *pspec)
{
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS:
      g_value_set_boolean (value, wifi_backend->wifi_status);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_wifi_set_wifi_status_property (GObject *object,
                                                   guint property_id,
                                                   const GValue *value,
                                                   GParamSpec *pspec)
{
  MildenhallSettingsWifiBackend *wifi_backend = MILDENHALL_SETTINGS_WIFI_BACKEND (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS:
      wifi_backend->wifi_status = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_wifi_backend_class_init (
    MildenhallSettingsWifiBackendClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = mildenhall_settings_wifi_backend_dispose;
  object_class->get_property = mildenhall_settings_wifi_get_wifi_status_property;
  object_class->set_property = mildenhall_settings_wifi_set_wifi_status_property;
  DEBUG ("entered");

  obj_properties[MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS] =
      g_param_spec_boolean ("wifi-status", "wifi-status", "Current WiFi status",
                            FALSE, G_PARAM_READABLE);
  g_object_class_install_property (
      object_class, MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS,
      obj_properties[MILDENHALL_SETTINGS_WIFI_BACKEND_WIFI_STATUS]);

  wifi_backend_signals[WIFI_SETTINGS_ERROR_SIGNAL] = g_signal_new (
      "wifi-settings-error-signal", G_TYPE_FROM_CLASS (object_class),
      G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST, 0,
      NULL,
      NULL, g_cclosure_marshal_generic, G_TYPE_NONE, 1, G_TYPE_STRING,
      NULL);
}

static void
mildenhall_settings_wifi_backend_init (MildenhallSettingsWifiBackend *wifi_backend)
{
  DEBUG ("entered");
  wifi_backend->cancellable = g_cancellable_new ();
  wifi_backend->connman_handler_id = g_bus_watch_name (G_BUS_TYPE_SYSTEM, "net.connman",
                                                       G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                                       wifi_net_connman_name_appeared,
                                                       wifi_net_connman_name_vanished,
                                                       wifi_backend, NULL);
}

MildenhallSettingsWifiBackend *
mildenhall_settings_wifi_backend_new (void)
{
  return g_object_new (MILDENHALL_SETTINGS_TYPE_WIFI_BACKEND, NULL);
}
