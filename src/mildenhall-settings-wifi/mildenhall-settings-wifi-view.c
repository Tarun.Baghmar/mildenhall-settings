/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 */

#include "mildenhall-settings-wifi-view.h"
#include "mildenhall-settings-wifi-internal.h"
#include "mildenhall-settings.h"
#include <mildenhall/mildenhall.h>
#include <thornbury/thornbury.h>

#define WIFI_SETTINGS_MENU_DRAWER_X_POS 600.0
#define WIFI_SETTINGS_MENU_DRAWER_Y_POS 395.0

enum
{
  WIFI_COLUMN_BD_NAME,
  WIFI_COLUMN_BD_ICON,
  WIFI_COLUMN_BD_TOOLTIP_TEXT,
  WIFI_COLUMN_BD_REACTIVE,
  WIFI_COLUMN_BD_LAST
};

typedef struct _MildenhallSettingsWifiView
{
  GObject parent;

  ClutterActor *wifi_window;    /* owned */
  ThornburyModel *drawer_model; /* owned */
  ClutterActor *drawer_button;  /* owned */
  gboolean state;               /* unowned */
} MildenhallSettingsWifiView;

G_DEFINE_TYPE (MildenhallSettingsWifiView, mildenhall_settings_wifi_view, G_TYPE_OBJECT)

static ThornburyModel *
mildenhall_settings_wifi_view_create_menu_drawer_model (void);

enum
{
  WIFI_VIEW_BUTTON_FIRST,
  WIFI_CONTEXT_DRAWER_RELEASED,
  WIFI_VIEW_BUTTON_LAST
};

enum MildenhallSettingsWifiViewProperties
{
  MILDENHALL_SETTINGS_WIFI_VIEW_ENUM_FIRST,
  MILDENHALL_SETTINGS_WIFI_VIEW_STATE,
  MILDENHALL_SETTINGS_WIFI_VIEW_ENUM_LAST
};

static GParamSpec *obj_properties[MILDENHALL_SETTINGS_WIFI_VIEW_ENUM_LAST] =
    {
      NULL,
    };
static guint32 mildenhall_settings_wifi_view_button_signals[WIFI_VIEW_BUTTON_LAST] =
    {
      0,
    };

static void
mildenhall_settings_wifi_view_dispose (GObject *object)
{
  MildenhallSettingsWifiView *wifi_view = MILDENHALL_SETTINGS_WIFI_VIEW (object);

  g_clear_object (&wifi_view->drawer_model);
  g_clear_pointer (&wifi_view->wifi_window, clutter_actor_destroy);
  G_OBJECT_CLASS (mildenhall_settings_wifi_view_parent_class)
      ->dispose (object);
}

static void
mildenhall_settings_wifi_get_wifi_state_property (GObject *object,
                                                  guint property_id,
                                                  GValue *value,
                                                  GParamSpec *pspec)
{
  MildenhallSettingsWifiView *wifi_view = MILDENHALL_SETTINGS_WIFI_VIEW (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_WIFI_VIEW_STATE:
      g_value_set_boolean (value, wifi_view->state);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_wifi_set_wifi_state_property (GObject *object,
                                                  guint property_id,
                                                  const GValue *value,
                                                  GParamSpec *pspec)
{
  MildenhallSettingsWifiView *wifi_view = MILDENHALL_SETTINGS_WIFI_VIEW (object);

  switch (property_id)
    {
    case MILDENHALL_SETTINGS_WIFI_VIEW_STATE:
      wifi_view->state = g_value_get_boolean (value);
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_wifi_view_class_init (MildenhallSettingsWifiViewClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  object_class->dispose = mildenhall_settings_wifi_view_dispose;
  object_class->get_property = mildenhall_settings_wifi_get_wifi_state_property;
  object_class->set_property = mildenhall_settings_wifi_set_wifi_state_property;

  obj_properties[MILDENHALL_SETTINGS_WIFI_VIEW_STATE] =
      g_param_spec_boolean ("state", "wifi-state", "Current WiFi state",
                            FALSE, G_PARAM_READWRITE);
  g_object_class_install_property (
      object_class, MILDENHALL_SETTINGS_WIFI_VIEW_STATE,
      obj_properties[MILDENHALL_SETTINGS_WIFI_VIEW_STATE]);

  mildenhall_settings_wifi_view_button_signals[WIFI_CONTEXT_DRAWER_RELEASED] = g_signal_new (
      "wifi-detailed-context-button-released", G_TYPE_FROM_CLASS (object_class),
      G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST, 0, NULL, NULL,
      g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING,
      NULL);
}

/*  Description  :  Create menu drawer model  */
static ThornburyModel *
mildenhall_settings_wifi_view_create_menu_drawer_model (void)
{
  ThornburyModel *thornbury_model =
      (ThornburyModel *) thornbury_list_model_new (WIFI_COLUMN_BD_LAST,
                                                   G_TYPE_STRING, NULL,
                                                   G_TYPE_STRING, NULL,
                                                   G_TYPE_STRING, NULL,
                                                   G_TYPE_BOOLEAN, NULL, -1);

  thornbury_model_append (thornbury_model,
                          WIFI_COLUMN_BD_NAME, WIFI_POWERED_STATUS,
                          WIFI_COLUMN_BD_ICON, WIFI_OFF_AC,
                          WIFI_COLUMN_BD_TOOLTIP_TEXT, WIFI_OFF,
                          WIFI_COLUMN_BD_REACTIVE, TRUE, -1);
  return thornbury_model;
}

/*  Description  :  Menu drawer button released callback  */
static void
mildenhall_settings_wifi_view_menu_drawer_button_released_cb (GObject *actor,
                                                              gchar *name,
                                                              gpointer user_data)
{
  MildenhallSettingsWifiView *wifi_view = MILDENHALL_SETTINGS_WIFI_VIEW (user_data);

  g_signal_emit (wifi_view,
                 mildenhall_settings_wifi_view_button_signals[WIFI_CONTEXT_DRAWER_RELEASED],
                 0, name);
}

/*  Update the menu drawer reactivity  */
static void
v_mildenhall_settings_wifi_view_change_menu_drawer_reactivity (ThornburyModel *model,
                                                               guint rows,
                                                               gboolean state)
{
  GValue icon_state = G_VALUE_INIT;
  ThornburyModelIter *thornbury_iter = thornbury_model_get_iter_at_row (model,
                                                                        rows);
  g_value_init (&icon_state, G_TYPE_BOOLEAN);
  g_value_set_boolean (&icon_state, state);
  thornbury_model_iter_set_value (thornbury_iter, WIFI_COLUMN_BD_REACTIVE,
                                  &icon_state);
  g_value_unset (&icon_state);
}

/*  Description  :  Callback used to update the HMI depends on wifi ON/OFF state  */
static void
mildenhall_settings_wifi_view_menu_drawer_state_change (GObject *gobject,
                                                        GParamSpec *pspec,
                                                        gpointer user_data)
{
  GValue temp_value = G_VALUE_INIT;
  MildenhallSettingsWifiView *wifi_view = MILDENHALL_SETTINGS_WIFI_VIEW (user_data);
  g_value_init (&temp_value, G_TYPE_STRING);
  DEBUG ("Entered");
  if (wifi_view->state)
    {
      g_value_set_static_string (&temp_value, WIFI_ON_AC);
      thornbury_model_insert_value (wifi_view->drawer_model, 0,
                                    WIFI_COLUMN_BD_ICON, &temp_value);
      g_value_set_static_string (&temp_value, WIFI_ON);
      thornbury_model_insert_value (wifi_view->drawer_model, 0,
                                    WIFI_COLUMN_BD_TOOLTIP_TEXT, &temp_value);
      v_mildenhall_settings_wifi_view_change_menu_drawer_reactivity (
          wifi_view->drawer_model, 0, TRUE);
    }
  else
    {
      g_value_set_static_string (&temp_value, WIFI_OFF_AC);
      thornbury_model_insert_value (wifi_view->drawer_model, 0,
                                    WIFI_COLUMN_BD_ICON, &temp_value);
      g_value_set_static_string (&temp_value, WIFI_OFF);
      thornbury_model_insert_value (wifi_view->drawer_model, 0,
                                    WIFI_COLUMN_BD_TOOLTIP_TEXT, &temp_value);
      v_mildenhall_settings_wifi_view_change_menu_drawer_reactivity (
          wifi_view->drawer_model, 0, TRUE);
    }
  g_value_unset (&temp_value);
}

/*  Description  :  creation of the Menu  Drawer  */
static void
v_mildenhall_settings_wifi_view_create_wifi_drawer (MildenhallSettingsWifiView *wifisettings)
{
  ThornburyItemFactory *button_drawer_item;
  MildenhallSettingsWifiView *wifi_view = MILDENHALL_SETTINGS_WIFI_VIEW (
      wifisettings);

  wifi_view->drawer_model = mildenhall_settings_wifi_view_create_menu_drawer_model ();
  button_drawer_item = thornbury_item_factory_generate_widget_with_props (
      MILDENHALL_DRAWER_TYPE_BASE,
      NULL);

  g_object_get (button_drawer_item, "object", &wifi_view->drawer_button, NULL);
  g_object_unref (button_drawer_item);
  g_object_set (wifi_view->drawer_button, "type", MILDENHALL_CONTEXT_DRAWER,
                "model", wifi_view->drawer_model, NULL);

  g_signal_connect (wifi_view->drawer_button,
                    "drawer-button-released",
                    G_CALLBACK (mildenhall_settings_wifi_view_menu_drawer_button_released_cb),
                    wifi_view);

  g_signal_connect (wifi_view,
                    "notify::state", G_CALLBACK (mildenhall_settings_wifi_view_menu_drawer_state_change),
                    wifi_view);
  clutter_actor_set_position (wifi_view->drawer_button, WIFI_SETTINGS_MENU_DRAWER_X_POS, WIFI_SETTINGS_MENU_DRAWER_Y_POS);
}

/*  Description  :  Creating the global widgets  */
static void
mildenhall_settings_wifi_view_create_widgets (MildenhallSettingsWifiView *self)
{
  MildenhallSettingsWifiView *wifi_view = MILDENHALL_SETTINGS_WIFI_VIEW (self);

  DEBUG ("entered");
  v_mildenhall_settings_wifi_view_create_wifi_drawer (wifi_view);
  clutter_actor_add_child (CLUTTER_ACTOR (wifi_view->wifi_window),
                           CLUTTER_ACTOR (wifi_view->drawer_button));
}

static void
mildenhall_settings_wifi_view_init (MildenhallSettingsWifiView *self)
{
  DEBUG ("entered");
  self->wifi_window = clutter_actor_new ();
  mildenhall_settings_wifi_view_create_widgets (self);
}

MildenhallSettingsWifiView *
mildenhall_settings_wifi_view_new (void)
{
  return g_object_new (MILDENHALL_SETTINGS_TYPE_WIFI_VIEW, NULL);
}

ClutterActor *
mildenhall_settings_wifi_get_view_container (MildenhallSettingsWifiView *view)
{
  g_return_val_if_fail (MILDENHALL_SETTINGS_WIFI_IS_VIEW (view), NULL);
  return view->wifi_window;
}

void
mildenhall_settings_wifi_show_view_with_name (MildenhallSettingsWifiView *view,
                                              const gchar *view_name)
{
  g_return_if_fail (MILDENHALL_SETTINGS_WIFI_IS_VIEW (view));
  if (g_strcmp0 (view_name, MILDENHALL_SETTINGS_WIFI_DEFAULT_VIEW) == 0 && (!clutter_actor_is_visible (view->wifi_window)))
    clutter_actor_show (view->wifi_window);
  return;
}
