/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 *  Copyright ©  2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _MILDENHALL_SETTINGS_MAIN_H
#define _MILDENHALL_SETTINGS_MAIN_H

#include <glib-object.h>
#include <mildenhall/mildenhall.h>

#include "mildenhall-settings-controller.h"

G_BEGIN_DECLS

#define MILDENHALL_SETTINGS_TYPE_MAIN mildenhall_settings_main_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallSettingsMain, mildenhall_settings_main, MILDENHALL_SETTINGS, MAIN, GObject)

MildenhallSettingsMain *mildenhall_settings_main_new (const gchar *app_name,
                                                      const gchar *launch_settings,
                                                      const gchar *view_name);

G_END_DECLS

#endif /* _MILDENHALL_SETTINGS_MAIN_H */
