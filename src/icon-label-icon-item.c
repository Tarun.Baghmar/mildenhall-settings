/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */

/*
 * Copyright © 2015 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

/*
 * icon_label_icon item for MildenhallRoller
 *
 *
 */

#include "icon-label-icon-item.h"

#include <mildenhall/mildenhall.h>
#include <thornbury/thornbury.h>

#include "liblightwood-expandable.h"
#include "liblightwood-fixedroller.h"
#include "liblightwood-glowshader.h"
#include "liblightwood-roller.h"

#define ROLLER_FONT(size) "DejaVuSansCondensed " #size "px"
#define ROLLER_ARROW_MARKUP "&#x25B8;"

static void icon_label_icon_item_expandable_iface_init (LightwoodExpandableIface *iface);

G_DEFINE_TYPE_WITH_CODE (IconLabelIconItem, icon_label_icon_item, CLUTTER_TYPE_GROUP, G_IMPLEMENT_INTERFACE (LIGHTWOOD_TYPE_EXPANDABLE, icon_label_icon_item_expandable_iface_init));

#define ICON_LABEL_ICON_ITEM_STATUS_TEXT_MAX_WIDTH 54.0
#define ICON_LABEL_ICON_ITEM_STATUS_TEXT_FONT "DejaVuSansCondensed 8px"
#define ICON_LABEL_ICON_ITEM_STATUS_TEXT_X 11.0
#define ICON_LABEL_ICON_ITEM_STATUS_TEXT_Y 3.0

#define ICON_LABEL_ICON_ITEM_RIGHT_STATUS_TEXT_X 11.0
#define ICON_LABEL_ICON_ITEM_VERTICAL_LINE4_X_DISPLACEMENT 68.0

#define ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_WIDTH 35.0
#define ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_HEIGHT 8.0
#define ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_X 20.0
#define ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_Y 50.0

static const ClutterColor NormalColor = { 0x98, 0xA9, 0xB2, 0xFF };

enum
{
  PROP_0,

  PROP_ICON,
  PROP_LABEL,
  PROP_ICON2,
  PROP_ARROW_UP,
  PROP_ARROW_DOWN,
  PROP_MODEL,
  PROP_ROW,
  PROP_FOCUSED,
  PROP_STATUS_LABEL,
  PROP_OPTIONS,
  PROP_PROGRESS
};

enum
{
  ICON_LABEL_ICON_NORMAL,
  ICON_LABEL_ICON_GREY_OUT
};

static void
icon_label_icon_item_get_property (GObject *object,
                                   guint property_id,
                                   GValue *value,
                                   GParamSpec *pspec)
{
  IconLabelIconItem *item = ICON_LABEL_ICON_ITEM (object);

  switch (property_id)
    {
    case PROP_ICON:
      break;
    case PROP_LABEL:
      g_value_set_string (value,
                          g_strdup (clutter_text_get_text (CLUTTER_TEXT (item->label))));
      break;
    case PROP_ICON2:
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
on_notify_language (GObject *object,
                    GParamSpec *pspec,
                    gpointer user_data)
{
  IconLabelIconItem *item = ICON_LABEL_ICON_ITEM (user_data);
  clutter_text_set_text (CLUTTER_TEXT (item->label), gettext (item->text));
}

static ClutterActor *
p_icon_label_icon_item_create_status_text (IconLabelIconItem *item, gfloat fltXPosition)
{
  ClutterActor *pStatusLabel = clutter_text_new ();
  clutter_text_set_single_line_mode (CLUTTER_TEXT (pStatusLabel), TRUE);
  clutter_text_set_color (CLUTTER_TEXT (pStatusLabel), &NormalColor);
  clutter_text_set_font_name (CLUTTER_TEXT (pStatusLabel), ICON_LABEL_ICON_ITEM_STATUS_TEXT_FONT);
  clutter_actor_add_child (CLUTTER_ACTOR (item), pStatusLabel);
  clutter_text_set_ellipsize (CLUTTER_TEXT (pStatusLabel), PANGO_ELLIPSIZE_END);
  clutter_text_set_justify (CLUTTER_TEXT (pStatusLabel), TRUE);
  clutter_text_set_line_alignment (CLUTTER_TEXT (pStatusLabel), PANGO_ALIGN_CENTER);
  clutter_actor_set_width (CLUTTER_ACTOR (pStatusLabel), ICON_LABEL_ICON_ITEM_STATUS_TEXT_MAX_WIDTH);
  clutter_actor_set_position (CLUTTER_ACTOR (pStatusLabel),
                              fltXPosition,
                              ICON_LABEL_ICON_ITEM_STATUS_TEXT_Y);
  return pStatusLabel;
}

static void
v_icon_label_icon_item_set_status_text (IconLabelIconItem *item, gchar *pStatusText)
{
  if (NULL == item->pStatusLabel)
    {
      item->pStatusLabel = p_icon_label_icon_item_create_status_text (item, ICON_LABEL_ICON_ITEM_STATUS_TEXT_X);
    }
  if (NULL == item->pStatusLabelRight)
    {
      gfloat roller_width, fltXposition = 0.0;
      ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (item));
      if (parent != NULL)
        {
          clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, NULL, &roller_width);
          fltXposition = roller_width - ICON_LABEL_ICON_ITEM_VERTICAL_LINE4_X_DISPLACEMENT + ICON_LABEL_ICON_ITEM_STATUS_TEXT_X;
        }
      item->pStatusLabelRight = p_icon_label_icon_item_create_status_text (item, fltXposition);
    }

  if (NULL == pStatusText)
    {
      pStatusText = g_strdup (" ");
    }
  clutter_text_set_text (CLUTTER_TEXT (item->pStatusLabel), pStatusText);
  clutter_text_set_text (CLUTTER_TEXT (item->pStatusLabelRight), pStatusText);
  if (NULL != pStatusText)
    {
      g_free (pStatusText);
      pStatusText = NULL;
    }
}

static ClutterActor *
p_icon_label_icon_item_create_loadingbar (IconLabelIconItem *item, gfloat fltXPosition)
{
  ClutterActor *pProgress = g_object_new (MILDENHALL_TYPE_LOADING_BAR,
                                          "loading-bar-width", ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_WIDTH,
                                          "loading-bar-height", ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_HEIGHT,
                                          "loading-bar-show", FALSE,
                                          "loading-bar-pos", 0.0,
                                          NULL);
  clutter_actor_add_child (CLUTTER_ACTOR (item), pProgress);
  clutter_actor_set_position (pProgress, fltXPosition, ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_Y);
  return pProgress;
}

static void
v_icon_label_icon_item_set_options (IconLabelIconItem *item, guint uinLaunchAction)
{
  if (uinLaunchAction == item->uinAdditionalOption)
    return;

  switch (uinLaunchAction)
    {
    case ICON_LABEL_ICON_GREY_OUT:
      {
        if (NULL != item->label)
          {
            //			clutter_text_set_color (CLUTTER_TEXT (item->label), &text_color);
            item->uinAdditionalOption = ICON_LABEL_ICON_GREY_OUT;
            if (clutter_actor_get_effect (item->icon, "glow") != NULL)
              clutter_actor_remove_effect_by_name (item->icon, "glow");
            if (clutter_actor_get_effect (item->label, "glow") != NULL)
              clutter_actor_remove_effect_by_name (item->label, "glow");
            if (clutter_actor_get_effect (item->icon2, "glow") != NULL)
              clutter_actor_remove_effect_by_name (item->icon2, "glow");
          }
      }
      break;
    case ICON_LABEL_ICON_NORMAL:
      {
        item->uinAdditionalOption = ICON_LABEL_ICON_NORMAL;
      }
      break;
    default:
      break;
    }
}

static void
v_icon_label_icon_item_set_progress (IconLabelIconItem *item, gfloat fltProgress)
{

  if (NULL == item->pProgress)
    {
      /* g_print("\n  creating Loading bar.... \n"); */
      item->pProgress = p_icon_label_icon_item_create_loadingbar (item, ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_X);
    }
  if (NULL == item->pProgressRight)
    {
      /* g_print("\n  creating Loading bar.... \n"); */

      gfloat roller_width, fltXposition = 0.0;
      ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (item));
      if (parent != NULL)
        {
          clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, NULL, &roller_width);
          fltXposition = roller_width - ICON_LABEL_ICON_ITEM_VERTICAL_LINE4_X_DISPLACEMENT + ICON_LABEL_ICON_ITEM_PROGRESS_TEXT_X;
        }

      item->pProgressRight = p_icon_label_icon_item_create_loadingbar (item, fltXposition);
    }
  g_object_set (item->pProgress, "loading-bar-pos", fltProgress, NULL);
  g_object_set (item->pProgress, "loading-bar-show", ((fltProgress > 0.0 && fltProgress < 1.0) ? TRUE : FALSE), NULL);

  g_object_set (item->pProgressRight, "loading-bar-pos", fltProgress, NULL);
  g_object_set (item->pProgressRight, "loading-bar-show", ((fltProgress > 0.0 && fltProgress < 1.0) ? TRUE : FALSE), NULL);
}

static void
icon_label_icon_item_set_property (GObject *object,
                                   guint property_id,
                                   const GValue *value,
                                   GParamSpec *pspec)
{
  IconLabelIconItem *item = ICON_LABEL_ICON_ITEM (object);

  switch (property_id)
    {
    case PROP_ICON:
      {
        if (g_value_get_string (value))
          {
            thornbury_ui_texture_set_from_file (item->icon, g_value_get_string (value), 0, 0, FALSE, TRUE);
            clutter_actor_show (item->icon);
          }
      }
      break;
    case PROP_LABEL:
      {
        ClutterActor *parent = NULL;
        gfloat roller_width;
        clutter_text_set_text (CLUTTER_TEXT (item->label), gettext ((gchar *) g_value_get_string (value)));
        clutter_text_set_ellipsize (CLUTTER_TEXT (item->label), PANGO_ELLIPSIZE_END);
        parent = clutter_actor_get_parent (CLUTTER_ACTOR (object));
        clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, NULL, &roller_width);
        clutter_actor_set_width (CLUTTER_ACTOR (item->label), roller_width - 180);
        item->text = g_strdup (g_value_get_string (value));
      }
      break;
    case PROP_ICON2:
      {
        if (g_value_get_string (value))
          {
            thornbury_ui_texture_set_from_file (item->icon2, g_value_get_string (value), 0, 0, FALSE, TRUE);
            clutter_actor_show (item->icon2);
          }
      }
      break;
    case PROP_ARROW_UP:
      if (g_value_get_int (value) == ILI_ARROW_HIDDEN)
        clutter_actor_hide (item->arrow_up);
      else
        {
          if (g_value_get_int (value) == ILI_ARROW_WHITE)
            clutter_actor_set_opacity (item->arrow_up, 255);
          else
            clutter_actor_set_opacity (item->arrow_up, 180);

          clutter_actor_show (item->arrow_up);
        }
      break;
    case PROP_ARROW_DOWN:
      if (g_value_get_int (value) == ILI_ARROW_HIDDEN)
        clutter_actor_hide (item->arrow_down);
      else
        {
          if (g_value_get_int (value) == ILI_ARROW_WHITE)
            clutter_actor_set_opacity (item->arrow_down, 255);
          else
            clutter_actor_set_opacity (item->arrow_down, 180);

          clutter_actor_show (item->arrow_down);
        }
      break;
    case PROP_MODEL:
      item->model = g_value_get_object (value);
      break;
    case PROP_ROW:
      {
        ClutterActor *parent = clutter_actor_get_parent (CLUTTER_ACTOR (object));
        gfloat roller_width;
        if (FALSE == item->row_called)
          {
            g_signal_connect (LIGHTWOOD_ROLLER (parent),
                              "notify::language",
                              G_CALLBACK (on_notify_language),
                              item);
          }

        item->row_called = TRUE;
        if (parent != NULL)
          {
            ThornburyModel *model = lightwood_roller_get_model (LIGHTWOOD_ROLLER (parent));
            clutter_actor_get_preferred_width (CLUTTER_ACTOR (parent), -1, NULL, &roller_width);
            clutter_actor_set_position (item->vertical_line1, roller_width - 1, 0);
            clutter_actor_set_position (item->vertical_line2, roller_width - 2, 0);
            clutter_actor_set_position (item->vertical_line3, roller_width - 66, 0);
            clutter_actor_set_position (item->vertical_line4, roller_width - ICON_LABEL_ICON_ITEM_VERTICAL_LINE4_X_DISPLACEMENT, 0);
            clutter_actor_set_position (item->icon2, roller_width - 48, 14);

            if (model != NULL &&
                g_value_get_uint (value) == (thornbury_model_get_n_rows (model) - 1))
              clutter_actor_show (item->extra_separator);
            else
              clutter_actor_hide (item->extra_separator);
          }
        break;
      }
    case PROP_FOCUSED:
      {
#if 1
        if (g_value_get_boolean (value))
          {
            //IconLabelIconItemClass *klass = ICON_LABEL_ICON_ITEM_GET_CLASS (object);
            if (clutter_actor_get_effect (item->icon, "glow") != NULL)
              return;

            if (ICON_LABEL_ICON_GREY_OUT != item->uinAdditionalOption)
              {
                clutter_actor_add_effect_with_name (item->icon, "glow", item->glow_effect_1);
                clutter_actor_add_effect_with_name (item->label, "glow", item->glow_effect_2);
                clutter_actor_add_effect_with_name (item->icon2, "glow", item->glow_effect_3);
              }
          }
        else
          {
            if (clutter_actor_get_effect (item->icon, "glow") == NULL)
              return;
            if (ICON_LABEL_ICON_GREY_OUT != item->uinAdditionalOption)
              {
                clutter_actor_remove_effect_by_name (item->icon, "glow");
                clutter_actor_remove_effect_by_name (item->label, "glow");
                clutter_actor_remove_effect_by_name (item->icon2, "glow");
              }
          }
#endif
      }
      break;

    case PROP_STATUS_LABEL:
      {
        gchar *pStatusText = g_value_dup_string (value);
        v_icon_label_icon_item_set_status_text (item, pStatusText);
      }
      break;
    case PROP_PROGRESS:
      {

        /* g_print("\n \t ICON_LABEL_ICON PROGRESSSSSS.......-----> %f    for item %s ",g_value_get_float (value) , clutter_text_get_text(CLUTTER_TEXT (item->label))); */
        gfloat fltProgress = g_value_get_float (value);
        v_icon_label_icon_item_set_progress (item, fltProgress);
      }
      break;
    case PROP_OPTIONS:
      {
        guint uinLaunchAction = g_value_get_uint (value);
        v_icon_label_icon_item_set_options (item, uinLaunchAction);
      }
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
icon_label_icon_item_dispose (GObject *obj)
{
  IconLabelIconItem *item = ICON_LABEL_ICON_ITEM (obj);

  g_clear_object (&item->glow_effect_1);
  g_clear_object (&item->glow_effect_2);
  g_clear_object (&item->glow_effect_3);

  G_OBJECT_CLASS (icon_label_icon_item_parent_class)
      ->dispose (obj);
}

/* LightwoodExpandable implementation */
#if 0

static void
icon_label_icon_item_set_expanded ( LightwoodExpandable *expandable,
                                    gboolean       expanded,
                                    gboolean       animated)
{
  if (expanded == ICON_LABEL_ICON_ITEM (expandable)->expanded)
    return;

  if (expanded)
    {
      if (animated)
        clutter_actor_animate (CLUTTER_ACTOR (expandable), CLUTTER_LINEAR, 1000,
                               "height", 100.0,
                               NULL);
      else
        clutter_actor_set_height (CLUTTER_ACTOR (expandable), 100);
    }
  else
    {
      if (animated)
        clutter_actor_animate (CLUTTER_ACTOR (expandable), CLUTTER_LINEAR, 1000,
                               "height", 64.0,
                               NULL);
      else
        clutter_actor_set_height (CLUTTER_ACTOR (expandable), -1);
    }

  ICON_LABEL_ICON_ITEM (expandable)->expanded = expanded;
}
#endif

static void
icon_label_icon_item_expandable_iface_init (LightwoodExpandableIface *iface)
{
  //iface->set_expanded = icon_label_icon_item_set_expanded;
}

static gboolean
button_release_event (ClutterActor *actor,
                      ClutterButtonEvent *event)
{
  ClutterActor *parent = clutter_actor_get_parent (actor);

  g_return_val_if_fail (LIGHTWOOD_IS_FIXED_ROLLER (parent), FALSE);

  g_debug ("actor '%s' has been activated, (un)expanding",
           clutter_actor_get_name (actor));

  /* FIXME: The callback, button_release_event, is disabled
     because of the issue T2996.
     Once lightwood_fixed_roller_set_expanded_item is called,
     the roller will lose its focus so no item can be activated. 
     The relevant task is T3001. */
  return FALSE;

  if (ICON_LABEL_ICON_ITEM (actor)->expanded)
    lightwood_fixed_roller_set_expanded_item (LIGHTWOOD_FIXED_ROLLER (parent), NULL);
  else
    lightwood_fixed_roller_set_expanded_item (LIGHTWOOD_FIXED_ROLLER (parent), actor);

  return FALSE;
}
static void
actor_allocate (ClutterActor *actor,
                const ClutterActorBox *box,
                ClutterAllocationFlags flags)
{
  CLUTTER_ACTOR_CLASS (icon_label_icon_item_parent_class)
      ->allocate (actor, box, flags);
}
static void
actor_paint (ClutterActor *actor)
{
  CLUTTER_ACTOR_CLASS (icon_label_icon_item_parent_class)
      ->paint (actor);
}

static void
icon_label_icon_item_class_init (IconLabelIconItemClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  ClutterActorClass *actor_class = CLUTTER_ACTOR_CLASS (klass);
  GParamSpec *pspec;

  object_class->get_property = icon_label_icon_item_get_property;
  object_class->set_property = icon_label_icon_item_set_property;
  object_class->dispose = icon_label_icon_item_dispose;

  actor_class->paint = actor_paint;
  actor_class->allocate = actor_allocate;

  actor_class->button_release_event = button_release_event;

  pspec = g_param_spec_string ("icon",
                               "icon",
                               "path for the icon",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ICON, pspec);

  pspec = g_param_spec_string ("label",
                               "label",
                               "Text for the label",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_LABEL, pspec);

  pspec = g_param_spec_string ("icon2",
                               "icon2",
                               "path for the icon2",
                               NULL,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_ICON2, pspec);

  pspec = g_param_spec_int ("arrow-up",
                            "arrow up",
                            "Controls the display of the upwards arrow",
                            ILI_ARROW_WHITE,
                            ILI_ARROW_LAST,
                            ILI_ARROW_HIDDEN,
                            G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_ARROW_UP, pspec);

  pspec = g_param_spec_int ("arrow-down",
                            "arrow down",
                            "Controls the display of the downwards arrow",
                            ILI_ARROW_WHITE,
                            ILI_ARROW_LAST,
                            ILI_ARROW_HIDDEN,
                            G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_ARROW_DOWN, pspec);

  pspec = g_param_spec_object ("model",
                               "ThornburyModel",
                               "ThornburyModel",
                               G_TYPE_OBJECT,
                               G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_MODEL, pspec);

  pspec = g_param_spec_uint ("row",
                             "row number",
                             "row number",
                             0, G_MAXUINT,
                             0,
                             G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_ROW, pspec);

  pspec = g_param_spec_boolean ("focused",
                                "focused",
                                "Whether this actor should be rendered as focused",
                                FALSE,
                                G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_FOCUSED, pspec);

  pspec = g_param_spec_uint ("extra-opt",
                             "extra-opt",
                             "additional operations that can be done item members",
                             0, G_MAXUINT,
                             0,
                             G_PARAM_WRITABLE);
  g_object_class_install_property (object_class, PROP_OPTIONS, pspec);

  /**
	 *	IconLabelIconItem: status-label:
	 *
	 * status Text to be displayed in Item type
	 */
  pspec = g_param_spec_string ("status-label", "status-label",
                               "status Text to be displayed in Item type", NULL, G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_STATUS_LABEL, pspec);

  /**
	 *	IconLabelIconItem: progress:
	 *
	 * progress to be displayed in Item type
	 * Default: NULL
	 */

  pspec = g_param_spec_float ("progress",
                              "progress",
                              "progress of the app being installed/downloaded",
                              0.0, G_MAXFLOAT,
                              0.0,
                              G_PARAM_READWRITE);
  g_object_class_install_property (object_class, PROP_PROGRESS, pspec);
}
static ClutterActor *
DrawVerticalSeperator (ClutterColor *lineColor, ClutterColor *depthLineColor, gfloat width, gfloat height)
{
  /*Create a separator line group */
  ClutterActor *separatorLineGroup = clutter_actor_new ();

  /*The main separator line */
  ClutterActor *separatorLine = clutter_actor_new (); //clutter_rectangle_new_with_color (lineColor);
  clutter_actor_set_background_color (separatorLine, lineColor);
  clutter_actor_set_position (separatorLine, 0, 0);
  clutter_actor_set_size (separatorLine, width, height);
  clutter_actor_add_child (CLUTTER_ACTOR (separatorLineGroup), separatorLine);

  /*Second line for the depth effect */
  if (NULL != depthLineColor)
    {
      ClutterActor *separatorLine2 = clutter_actor_new (); //clutter_rectangle_new_with_color (lineColor);
      clutter_actor_set_background_color (separatorLine2, depthLineColor);
      clutter_actor_set_position (separatorLine2, 1, 0);
      clutter_actor_set_size (separatorLine2, width, height);
      clutter_actor_add_child (CLUTTER_ACTOR (separatorLineGroup), separatorLine2);
    }

  /*Return the separator line group that gets addded to the roller background */
  return separatorLineGroup;
}

static void
icon_label_icon_item_init (IconLabelIconItem *icon_label_icon)
{
  g_autofree gchar *arrow_up_img_file = NULL;
  g_autofree gchar *arrow_down_img_file = NULL;
  ClutterActor *line;
  ClutterColor text_color = { 0x98, 0XA9, 0XB2, 0XFF }; //{ 255, 255, 255, 255 };
  ClutterColor line_horizon_color = { 0xFF, 0xFF, 0xFF, 0x33 };
  ClutterColor line_color = { 0x00, 0x00, 0x00, 0x66 };
  ClutterColor depthLineColor = { 0xFF, 0xFF, 0xFF, 0x33 };
  ClutterActor *sepLine = NULL;

  //	printf("item creation\n");
  icon_label_icon->pProgress = NULL;
  icon_label_icon->pProgressRight = NULL;
  icon_label_icon->pStatusLabel = NULL;
  icon_label_icon->pStatusLabelRight = NULL;
  icon_label_icon->uinAdditionalOption = 0;
  icon_label_icon->row_called = FALSE;

  arrow_up_img_file = g_strdup (PKGDATADIR "/arrow-up.png");

  arrow_down_img_file = g_strdup (PKGDATADIR "/arrow-down.png");

  icon_label_icon->icon = thornbury_ui_texture_create_new (
      arrow_up_img_file, 0, 0, FALSE, FALSE);

  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->icon);
  clutter_actor_set_position (icon_label_icon->icon, 18, 14);
  clutter_actor_hide (icon_label_icon->icon);

  icon_label_icon->icon2 = thornbury_ui_texture_create_new (
      arrow_up_img_file, 0, 0, FALSE, FALSE);

  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->icon2);
  //clutter_actor_set_position (icon_label_icon->icon2, 406, 14);
  clutter_actor_hide (icon_label_icon->icon2);

  icon_label_icon->arrow_up = thornbury_ui_texture_create_new (
      arrow_up_img_file, 0, 0, FALSE, FALSE);

  clutter_actor_set_position (icon_label_icon->arrow_up, 34, 10);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->arrow_up);
  clutter_actor_hide (icon_label_icon->arrow_up);

  icon_label_icon->arrow_down = thornbury_ui_texture_create_new (
      arrow_down_img_file, 0, 0, FALSE, FALSE);

  clutter_actor_set_position (icon_label_icon->arrow_down, 34, 53);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->arrow_down);
  clutter_actor_hide (icon_label_icon->arrow_down);

  icon_label_icon->label = clutter_text_new ();
  clutter_text_set_color (CLUTTER_TEXT (icon_label_icon->label), &text_color);
  clutter_text_set_font_name (CLUTTER_TEXT (icon_label_icon->label), ROLLER_FONT (28));
  clutter_actor_set_position (icon_label_icon->label, 79, 15);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->label);
  clutter_actor_show (icon_label_icon->label);

  /* Vertical separators */
  sepLine = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 64);
  clutter_actor_set_position (sepLine, 5, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), sepLine);
  clutter_actor_show (sepLine);

  sepLine = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 64);
  clutter_actor_set_position (sepLine, 64, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), sepLine);
  clutter_actor_show (sepLine);

  icon_label_icon->vertical_line1 = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 64);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->vertical_line1);

  icon_label_icon->vertical_line2 = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 64);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->vertical_line2);

  icon_label_icon->vertical_line3 = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 64);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->vertical_line3);

  icon_label_icon->vertical_line4 = DrawVerticalSeperator (&line_color, &depthLineColor, 1, 64);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), icon_label_icon->vertical_line4);

  /* Horizontal separators */
  line = clutter_actor_new ();                                    //clutter_rectangle_new_with_color (lineColor);
  clutter_actor_set_background_color (line, &line_horizon_color); //clutter_rectangle_new_with_color (&line_horizon_color);
  clutter_actor_set_size (line, 57, 1);
  clutter_actor_set_position (line, 7, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), line);
  clutter_actor_show (line);

  line = clutter_actor_new ();                                    //clutter_rectangle_new_with_color (lineColor);
  clutter_actor_set_background_color (line, &line_horizon_color); //clutter_rectangle_new_with_color (&line_horizon_color);
  clutter_actor_set_size (line, 729, 1);
  clutter_actor_set_position (line, 66, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon), line);
  clutter_actor_show (line);

  /* Extra horizontal separators */
  icon_label_icon->extra_separator = clutter_actor_new ();
  clutter_actor_set_position (icon_label_icon->extra_separator, 0, 62);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon),
                           icon_label_icon->extra_separator);

  line = clutter_actor_new ();                                    //clutter_rectangle_new_with_color (lineColor);
  clutter_actor_set_background_color (line, &line_horizon_color); //clutter_rectangle_new_with_color (&line_horizon_color);
  clutter_actor_set_size (line, 57, 1);
  clutter_actor_set_position (line, 7, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon->extra_separator),
                           line);

  clutter_actor_show (line);

  line = clutter_actor_new (); //clutter_rectangle_new_with_color (lineColor);
  clutter_actor_set_background_color (line, &line_horizon_color);
  clutter_actor_set_size (line, 729, 1);
  clutter_actor_set_position (line, 66, 0);
  clutter_actor_add_child (CLUTTER_ACTOR (icon_label_icon->extra_separator),
                           line);
  clutter_actor_show (line);

  clutter_actor_set_reactive (CLUTTER_ACTOR (icon_label_icon), TRUE);
  icon_label_icon->glow_effect_1 = g_object_ref_sink (lightwood_glow_shader_new ());
  icon_label_icon->glow_effect_2 = g_object_ref_sink (lightwood_glow_shader_new ());
  icon_label_icon->glow_effect_3 = g_object_ref_sink (lightwood_glow_shader_new ());
}

ClutterActor *
icon_label_icon_item_new (void)
{
  return g_object_new (TYPE_ICON_LABEL_ICON_ITEM, NULL);
}
