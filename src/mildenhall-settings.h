/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef MILDENHALL_SETTINGS_H_
#define MILDENHALL_SETTINGS_H_

#define DEBUG(format, ...) g_debug ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define CRITICAL(format, ...) g_critical ("%s: " format, G_STRFUNC, ##__VA_ARGS__)
#define WARNING(format, ...) g_warning ("%s: " format, G_STRFUNC, ##__VA_ARGS__)

#endif /* MILDENHALL_SETTINGS_DEBUG_H_ */

