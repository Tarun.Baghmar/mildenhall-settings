/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef _MILDENHALL_SETTINGS_VIEW_H
#define _MILDENHALL_SETTINGS_VIEW_H

#include <stdlib.h>

#include <clutter/clutter.h>
#include <glib-object.h>
#include <icon-label-icon-item.h>
#include <mildenhall/mildenhall.h>
#include <thornbury/thornbury.h>

#include "mildenhall-settings.h"

G_BEGIN_DECLS

#define MILDENHALL_SETTINGS_TYPE_VIEW mildenhall_settings_view_get_type ()
G_DECLARE_FINAL_TYPE (MildenhallSettingsView, mildenhall_settings_view, MILDENHALL_SETTINGS, VIEW, GObject)

typedef struct _MildenhallSettingsView
{
  GObject parent;
  ThornburyModel *main_roller_model; /* owned */
  ClutterActor *main_container;      /* unowned */
  ClutterActor *second_container;    /* unowned */
  ClutterActor *main_roller;         /* unowned */
  ClutterActor *main_window;         /* owned */
  ClutterActor *settings_bkg;        /* unowned */
} MildenhallSettingsView;

enum
{
  ROLLER_COLUMN_LABEL,
  ROLLER_COLUMN_LEFT_ICON,
  ROLLER_COLUMN_RIGHT_ICON,
  ROLLER_COLUMN_MODULE_NAME,
  ROLLER_COLUMN_GET_TYPE_FUNC,
  ROLLER_COLUMN_APP_SETTING,
  ROLLER_COLUMN_LAST
};

MildenhallSettingsView *mildenhall_settings_view_new (void);
G_END_DECLS

#endif /*_MILDENHALL_SETTINGS_VIEW_H */
