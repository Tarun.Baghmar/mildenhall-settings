/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Robert Bosch Car Multimedia GmbH
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "mildenhall-settings-client-handler.h"

G_DEFINE_TYPE (MildenhallSettingsClientHandler,
	       mildenhall_settings_client_handler, G_TYPE_OBJECT)

enum
{
  MILDENHALL_CLIENT_HANDLER_GET_APP_SETTINGS_DATABASE_LIST,
  MILDENHALL_CLIENT_HANDLER_RESPONSE_BACK_PRESS,
  MILDENHALL_CLIENT_HANDLER_SIGNAL_LAST
};

enum
{
  MILDENHALL_SETTINGS_CLIENT_HANDLER_APP_NAME = 1,
  MILDENHALL_SETTINGS_CLIENT_HANDLER_APP_DATABASE_LIST,
  MILDENHALL_SETTINGS_CLIENT_HANDLER_ENUM_LAST
};

static guint32 mildenhall_settings_client_handler_signals[MILDENHALL_CLIENT_HANDLER_SIGNAL_LAST] = { 0, };
static void
mildenhall_settings_client_handler_app_mgr_name_vanished (GDBusConnection * connection,
                                                          const gchar * name,
                                                          gpointer user_data);
static void
mildenhall_settings_client_handler_app_mgr_name_appeared (GDBusConnection * connection,
                                                          const gchar * name,
                                                          const gchar * name_owner,
                                                          gpointer user_data);

static void
mildenhall_settings_client_handler_dispose (GObject *object)
{
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (object);

  g_clear_object (&self->app_mgr_proxy);
  g_slist_free_full (self->list_of_app_data, g_free);
  g_clear_object (&self->hardkeys_mgr_proxy);
  if (self->watcher_id != 0)
    {
      g_bus_unwatch_name (self->watcher_id);
      self->watcher_id = 0;
    }
  G_OBJECT_CLASS (mildenhall_settings_client_handler_parent_class)->dispose (
      object);
}

static void
mildenhall_settings_client_handler_finalize (GObject *object)
{
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (object);

  g_clear_pointer (&self->app_name, g_free);
  G_OBJECT_CLASS (mildenhall_settings_client_handler_parent_class)->finalize (
      object);
}

static void
mildenhall_settings_client_handler_get_property (GObject *object,
                                                 guint property_id,
                                                 GValue *value,
                                                 GParamSpec *pspec)
{
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (object);

  switch (property_id)
  {
    case MILDENHALL_SETTINGS_CLIENT_HANDLER_APP_NAME:
      g_value_set_string (value, self->app_name);
      break;
    case MILDENHALL_SETTINGS_CLIENT_HANDLER_APP_DATABASE_LIST:
      g_value_set_pointer (value, self->list_of_app_data);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
  }
}

static void
mildenhall_settings_client_handler_set_property (GObject *object,
                                                 guint property_id,
                                                 const GValue *value,
                                                 GParamSpec *pspec)
{
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (object);

  switch (property_id)
    {
      case MILDENHALL_SETTINGS_CLIENT_HANDLER_APP_NAME:
	self->app_name = g_value_dup_string (value);
	break;
      default:
	G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
    }
}

static void
mildenhall_settings_client_handler_class_init (MildenhallSettingsClientHandlerClass *klass)
{
  GParamSpec *pspec = NULL;
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->get_property = mildenhall_settings_client_handler_get_property;
  object_class->set_property = mildenhall_settings_client_handler_set_property;
  object_class->dispose = mildenhall_settings_client_handler_dispose;
  object_class->finalize = mildenhall_settings_client_handler_finalize;

  pspec = g_param_spec_string ("app-name", "app-name", "Settings App Name", NULL ,
                              (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT_ONLY));
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_CLIENT_HANDLER_APP_NAME, pspec);

  pspec = g_param_spec_pointer ("app-database-list", "app-database-list", "Settings App Database List", G_PARAM_READABLE);
  g_object_class_install_property (object_class, MILDENHALL_SETTINGS_CLIENT_HANDLER_APP_DATABASE_LIST, pspec);
  
  mildenhall_settings_client_handler_signals[MILDENHALL_CLIENT_HANDLER_RESPONSE_BACK_PRESS] =
      g_signal_new (
	  "response-back-press",
	  G_TYPE_FROM_CLASS (object_class),
	  G_SIGNAL_NO_RECURSE | G_SIGNAL_RUN_LAST,
	  0,
	  NULL,
	  NULL,
	  g_cclosure_marshal_VOID__STRING, G_TYPE_NONE, 1, G_TYPE_STRING);
}

static void
mildenhall_settings_client_handler_init (MildenhallSettingsClientHandler *self)
{
  self->app_mgr_proxy = NULL;
  self->hardkeys_mgr_proxy = NULL;
  self->watcher_id = g_bus_watch_name (G_BUS_TYPE_SESSION, "org.apertis.Canterbury",
                                       G_BUS_NAME_WATCHER_FLAGS_AUTO_START,
                                       mildenhall_settings_client_handler_app_mgr_name_appeared,
                                       mildenhall_settings_client_handler_app_mgr_name_vanished,
                                       self, NULL);
}

MildenhallSettingsClientHandler *
mildenhall_settings_client_handler_new (const gchar *app_name)
{
  return g_object_new (MILDENHALL_SETTINGS_TYPE_CLIENT_HANDLER, "app-name", app_name, NULL);
}

static void
mildenhall_settings_client_handler_hard_keys_back_press_status (CanterburyHardKeys * object,
                                                                const gchar *arg_app_name,
                                                                gpointer user_data)
{
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (user_data);
  g_signal_emit (self,
                 mildenhall_settings_client_handler_signals[MILDENHALL_CLIENT_HANDLER_RESPONSE_BACK_PRESS],
                 0,
                 arg_app_name);
}

static void
mildenhall_settings_client_handler_hard_keys_proxy_clb (GObject * source_object,
                                                        GAsyncResult * res,
                                                        gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (user_data);

  /* finishes the proxy creation and gets the proxy ptr */
  self->hardkeys_mgr_proxy = canterbury_hard_keys_proxy_new_finish (res,
								    &error);

  if (self->hardkeys_mgr_proxy == NULL)
    {
      WARNING ("hardkey proxy creation failed with error: %s", error->message);
      g_clear_error (&error);
      return;
    }

  g_signal_connect (self->hardkeys_mgr_proxy,
                    "back-press-register-response",
                    G_CALLBACK (mildenhall_settings_client_handler_hard_keys_back_press_status),
                    self);

  canterbury_hard_keys_call_register_back_press_sync (self->hardkeys_mgr_proxy,
						      self->app_name,
						      NULL,
						      NULL);
  g_clear_error (&error);
}

static void
mildenhall_settings_client_handler_hard_keys_name_appeared (GDBusConnection * connection,
                                                            const gchar * name,
                                                            const gchar * name_owner,
                                                            gpointer user_data)
{
  /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_hard_keys_proxy_new (
      connection, G_DBUS_PROXY_FLAGS_NONE, "org.apertis.Canterbury",
      "/org/apertis/Canterbury/HardKeys",
      NULL,
      mildenhall_settings_client_handler_hard_keys_proxy_clb, user_data);
}

static void
mildenhall_settings_client_handler_hard_keys_name_vanished (GDBusConnection * connection,
                                                            const gchar * name,
                                                            gpointer user_data)
{
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (user_data);

  g_clear_object (&self->hardkeys_mgr_proxy);
}

static void
mildenhall_settings_client_handler_register_clb (GObject * source_object,
						 GAsyncResult * res,
						 gpointer user_data)
{
  gboolean return_val;
  GError *error = NULL;
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (user_data);

  return_val = canterbury_app_manager_call_register_my_app_finish (
      self->app_mgr_proxy, res, &error);

  if (return_val == FALSE)
    {
      WARNING ("Failed to register app (reason: %s)", error != NULL ? error->message : "");
    }

  g_clear_error (&error);
}

static void
mildenhall_settings_client_handler_app_mgr_proxy_clb (GObject * source_object,
						      GAsyncResult * res,
						      gpointer user_data)
{
  GError *error = NULL;
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (user_data);
  /* finishes the proxy creation and gets the proxy ptr */
  self->app_mgr_proxy = canterbury_app_manager_proxy_new_finish (res, &error);

  if (self->app_mgr_proxy == NULL)
    {
      if (error != NULL)
	{
	  WARNING ("app manager proxy creation failed with error: %s",
	           error->message);
	  g_clear_error (&error);
	}
      return;
    }

  canterbury_app_manager_call_register_my_app (
      self->app_mgr_proxy, self->app_name, 0 /* unused */,
      NULL,
      mildenhall_settings_client_handler_register_clb, user_data);

  g_clear_error (&error);
}

static void
mildenhall_settings_client_handler_app_mgr_name_appeared (GDBusConnection * connection,
                                                          const gchar * name,
                                                          const gchar * name_owner,
                                                          gpointer user_data)
{
  /* Asynchronously creates a proxy for the App manager D-Bus interface */
  canterbury_app_manager_proxy_new (
      connection, G_DBUS_PROXY_FLAGS_NONE, "org.apertis.Canterbury",
      "/org/apertis/Canterbury/AppManager",
      NULL,
      mildenhall_settings_client_handler_app_mgr_proxy_clb, user_data);

  mildenhall_settings_client_handler_hard_keys_name_appeared (connection, name,
							      name_owner,
							      user_data);
}

static void
mildenhall_settings_client_handler_app_mgr_name_vanished (GDBusConnection * connection,
                                                          const gchar * name,
                                                          gpointer user_data)
{
  MildenhallSettingsClientHandler *self = MILDENHALL_SETTINGS_CLIENT_HANDLER (user_data);

  g_clear_object (&self->app_mgr_proxy);
  mildenhall_settings_client_handler_hard_keys_name_vanished (connection, name,
							      user_data);
}
